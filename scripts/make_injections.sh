#!/bin/bash
injrange=500
gpsstart=`python -c "print 1167559936.6-0.5*${injrange}"`
gpsstop=`python -c "print 1167559936.6+0.5*${injrange}"`

./cbcBayesMapPosToSimInspiral.py /home/jclark/Projects/reconstruction-comparisons/gw170104/C01/Prod1Nest_posterior_samples.dat \
    --output Prod1Nest_${gpsstart}-${gpsstop}.xml \
    --gps-start ${gpsstart} --gps-end ${gpsstop}

./cbcBayesMapPosToSimInspiral.py /home/jclark/Projects/reconstruction-comparisons/gw170104/C01/Prod1MCMC_posterior_samples.dat \
    --output Prod1MCMC_${gpsstart}-${gpsstop}.xml \
    --gps-start ${gpsstart} --gps-end ${gpsstop}
