#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2017-2018 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""
Load and compare ascii-format whitened injections produced by
pycbc_generate_hwinj, BayesWave and LALInference.  Useful for checking phase
shifts, reproducibility etc.

"""

import sys, os
import numpy as np
from scipy import signal
import matplotlib
matplotlib.use("Agg")
from matplotlib import pyplot as plt

import argparse

import pycbc.types

import lalsimulation as lalsim
import gw_reconstruct as gwr


def parser():
    """ 
    Parser for input (command line and ini file)
    """

    # --- cmd line
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument("--srate", type=int, default=2048)
    parser.add_argument("--epoch", type=float, default=1167559934.6)
    parser.add_argument("--trigtime", type=float, default=1167559936.6)
    parser.add_argument("--duration", type=float, default=4.0)
    parser.add_argument("--make-plots", default=False, action="store_true")
    parser.add_argument("--bw-dir", default=None)
    parser.add_argument("--hwinj-pattern", default=None)
    parser.add_argument("--phase-shift", default=False, action="store_true")

    opts = parser.parse_args()

    return opts

opts = parser()

ifos = ['H1', 'L1']


# Load PSDs for whitening
psd_infile_fmt = os.path.join(opts.bw_dir, 'IFO{}_psd.dat')
infiles = [psd_infile_fmt.format(ifo) for ifo in xrange(len(ifos))]
psds = [gwr.psd.interp_from_txt(infile, flow=10) for infile in infiles]


# Ensure unit amplitude noise
seglen = int(opts.srate*opts.duration)
fac = 1./opts.srate * np.sqrt(float(seglen)/2)

# Read and whiten pycbc HWINJ
#hwinjcbcsimid9_1167559957_H1
infile_fmt = opts.hwinj_pattern+'_{}.txt'
hwinj_data = [np.loadtxt(infile_fmt.format(ifo)) for ifo in ifos]
hwinj_start = int(opts.hwinj_pattern.split('_')[1])
hwinj_times = np.arange(hwinj_start,hwinj_start+len(hwinj_data[0])*1./opts.srate, 1./opts.srate)
idx=range(int(0.5*len(hwinj_data[0])-0.5*seglen),int(0.5*len(hwinj_data[0])+0.5*seglen))
hwinj_epoch = hwinj_times[idx][0]
hwinj_data = [hdata[idx] for hdata in hwinj_data]
hwinj_hfs = [gwr.Strain(pycbc.types.TimeSeries(hw, delta_t=1./opts.srate,
    epoch=hwinj_epoch), white=False) for hw in hwinj_data]
hwinj_hts = [h.to_timeseries() for h in hwinj_hfs]
hwinj_hts_white = [fac*gwr.whiten_strain(hwinj, psd).to_timeseries()
        for hwinj,psd in zip(hwinj_hts,psds)]

phase_shift = np.exp(1j*np.pi/2)

# Read and whiten LI Injections
inj_infile_fmt = os.path.join(opts.bw_dir, 'injection_{}.dat')
infiles = [inj_infile_fmt.format(ifo) for ifo in ifos]
li_inj_data = [np.loadtxt(infile) for infile in infiles]
li_freq_range = li_inj_data[0][:,0]
phase_shift = np.exp(1j*2*np.pi)
delta_f = np.diff(li_freq_range)[0]
full_freq_range = np.arange(0,li_inj_data[0][-1,0]+delta_f, delta_f)
li_inj_hts_white = []

for i in xrange(len(ifos)):
    idx = np.where(full_freq_range >= min(li_freq_range))[0][0]
    tmp = np.zeros(len(full_freq_range),dtype=complex)
    tmp[idx:] = li_inj_data[i][:,1]+1j*li_inj_data[i][:,2]
    if opts.phase_shift: tmp *= phase_shift
    li_inj_hfs = gwr.Strain(pycbc.types.FrequencySeries(tmp, delta_f=delta_f,
        epoch=opts.epoch))
    li_inj_hfs_white = gwr.whiten_strain(li_inj_hfs, psds[i])
    li_inj_hts_white.append(fac*li_inj_hfs_white.to_timeseries())



# Read BW Injections
inj_infile_fmt = os.path.join(opts.bw_dir, 'injected_whitened_waveform.dat.{}')
infiles = [inj_infile_fmt.format(i) for i in xrange(len(ifos))]
bw_inj_data = [np.loadtxt(infile) for infile in infiles]

bw_inj_hts_white = [pycbc.types.TimeSeries(bw, delta_t=1./opts.srate,
    epoch=opts.epoch) for bw in bw_inj_data ]

z=np.exp(1j*np.pi/2)
if opts.phase_shift:
    for i in xrange(len(ifos)):
        tmp = bw_inj_hts_white[i].to_frequencyseries()
        bw_inj_hts_white[i] = (tmp*z).to_timeseries()

#
# Overlaps/matches
#

psd_infile_fmt = os.path.join(opts.bw_dir, 'IFO{}_psd.dat')
infiles = [psd_infile_fmt.format(ifo) for ifo in xrange(len(ifos))]
psd_data = [np.loadtxt(infile) for infile in infiles]
pypsds = [pycbc.types.FrequencySeries(np.interp(full_freq_range, pdata[:,0],
    pdata[:,1]), delta_f=0.25) for pdata in psd_data]

bw_inj_hts = [gwr.unwhiten_strain(gwr.Strain(bw, white=True),
    psd).to_timeseries() for bw,psd in zip(bw_inj_hts_white, psds)]
hw_inj_hts = [gwr.unwhiten_strain(gwr.Strain(hw, white=True),
    psd).to_timeseries() for hw,psd in zip(hwinj_hts_white, psds)]

match = [pycbc.filter.match(bw, hw,  low_frequency_cutoff=20, psd=p) for bw, hw,
        p in zip(bw_inj_hts_white, hwinj_hts_white, pypsds)]
overlap = [pycbc.filter.overlap(bw, hw,  low_frequency_cutoff=20, psd=p) for bw,
        hw, p in zip(bw_inj_hts_white, hwinj_hts_white, pypsds)]

match = [pycbc.filter.match(bw, hw,  low_frequency_cutoff=20, psd=p) for bw, hw,
        p in zip(bw_inj_hts, hwinj_hts, pypsds)]
overlap = [pycbc.filter.overlap(bw, hw,  low_frequency_cutoff=20, psd=p) for bw,
        hw, p in zip(bw_inj_hts, hwinj_hts, pypsds)]

hw_local_times = hwinj_hts_white[0].sample_times - opts.trigtime
li_local_times = li_inj_hts_white[0].sample_times - (opts.trigtime)# + 2./2048)
bw_local_times = bw_inj_hts_white[0].sample_times - opts.trigtime #+ 1./2048

f, ax = plt.subplots(nrows=2,sharex=True)
for i in xrange(len(ifos)):
    ax[i].plot(hw_local_times, hwinj_hts_white[i], linewidth=1, label='pycbc injection')
#    ax[i].plot(li_local_times, li_inj_hts_white[i], linewidth=1, label='LI')
    ax[i].plot(bw_local_times, bw_inj_hts_white[i], linewidth=1, label='bayeswave (LI) injection')
    ax[i].legend()
    ax[i].set_xlim(-0.1,0.05)
    ax[i].set_ylabel('Whitened %s h(t)'%ifos[i])
    ax[i].set_title("Match={:.2f}".format( float(match[i][0])))

ax[1].set_xlabel('Time [s]')

plt.show()

